package com.edsolabs.pawnservice.repository;

import com.edsolabs.pawnservice.entity.Collateral;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.swing.text.html.HTMLDocument;
import javax.transaction.Transactional;
import java.util.List;


@Repository
public interface CollateralRepository extends JpaRepository<Collateral,Long>, JpaSpecificationExecutor<Collateral> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE Collateral SET Collateral.status = 'WITHDRAWN' WHERE Collateral.id = ?1",nativeQuery = true)
    void withdrawnCollateral(long id) ;


    Page<Collateral> findAll(Pageable pageable) ;

//    Page<Collateral> findAll(Pageable pageable) ;
//
//    List<Collateral> findALL(Specification<Collateral> spec, Pageable pageable) ;



}
