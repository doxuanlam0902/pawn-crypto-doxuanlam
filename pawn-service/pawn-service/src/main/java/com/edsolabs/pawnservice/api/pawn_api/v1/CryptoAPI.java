package com.edsolabs.pawnservice.api.pawn_api.v1;

import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.entity.Crypto;
import com.edsolabs.pawnservice.service.CryptoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/crypto")
@CrossOrigin
@Slf4j
public class CryptoAPI {

    @Autowired
    CryptoService cryptoService ;

    @GetMapping("/find/{id}")
    public CryptoDTO findCryptoById(@PathVariable long id) {
        return cryptoService.findCryptoById(id) ;
    }

    @GetMapping("/cryptos")
    public List<CryptoDTO> findAll(){
        return cryptoService.findALl() ;
    }
}
