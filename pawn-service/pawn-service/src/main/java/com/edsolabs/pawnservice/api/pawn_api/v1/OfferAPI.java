package com.edsolabs.pawnservice.api.pawn_api.v1;

import com.edsolabs.pawnservice.StatusCollaterals;
import com.edsolabs.pawnservice.StatusOffer;
import com.edsolabs.pawnservice.api.output.ApplicationException;
import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.dto.OfferDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Offer;
import com.edsolabs.pawnservice.service.CollateralService;
import com.edsolabs.pawnservice.service.OfferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/api/v1")

public class OfferAPI {

    @Autowired
    OfferService offerService ;
    @Autowired
    CollateralService collateralService ;

    /**
     * tạo mới một Offer
     * @param
     * @return
     */
    @PostMapping("/offers")
    public OfferDTO createOffer(@RequestParam(name = "lender") String lender ,
                             @RequestParam(name = "message") String message,
                             @RequestParam(name = "loanToValue") double loanToValue,
                             @RequestParam(name = "loanAmountId") long loanAmountId,
                             @RequestParam(name = "interestRate") int interestRate,
                             @RequestParam(name = "LTV") double LTV,
                             @RequestParam(name = "duration") int duration,
                             @RequestParam(name = "repaymentCurrency") String repaymentCurrency,
                             @RequestParam(name = "recurringInterest") String recurringInterest,
                                @RequestParam(name = "collateralId") long collateralId
                             ){
        RestTemplate restTemplate = new RestTemplate();
        OfferDTO offerDTO = new OfferDTO() ;
        // kiểm tra lender
        if(lender != "") {
            offerDTO.setLender(lender);
        }else {
            throw new ApplicationException(404,"lender is not validate") ;
        }
        // kiểm tra message
        if(message != "") {
            offerDTO.setMessage(message);
        }else {
            throw new ApplicationException(404,"message of offer is not validate") ;
        }
        // kiểm tra Loan To Value
        if(loanToValue > 0) {
            offerDTO.setLoanToValue(loanToValue);
        }else {
            throw new ApplicationException(404,"Loan to value is not validate") ;
        }

        // lấy crypto theo id cho Loan Amount
        CryptoDTO cryptoDTO = restTemplate.getForObject(
                "http://localhost:9000/api/v1/crypto/find/" + loanAmountId ,
                CryptoDTO.class);
        if(cryptoDTO != null) {
            offerDTO.setLoanAmount(cryptoDTO);
        } else {
            throw  new ApplicationException(404,"Crypto of loan amount is not validate") ;
        }
        // kiểm tra interest Rate
        if(interestRate >0) {
            offerDTO.setInterestRate(interestRate);
        } else {
            throw  new ApplicationException(404,"Interest Rate is not validate") ;
        }
        // kiểm tra LTV
        if(LTV >0) {
            offerDTO.setLTV(LTV);
        } else {
            throw  new ApplicationException(404,"LTV is not validate") ;
        }
        // kiểm tra duration
        if(duration >0) {
            offerDTO.setDuration(duration);
        } else {
            throw  new ApplicationException(404,"Duration of offer is not validate") ;
        }
        // Kiểm tra repayment currency
        if(repaymentCurrency != "") {
            offerDTO.setRepaymentCurrency(repaymentCurrency);
        } else {
            throw  new ApplicationException(404,"Repayment Currency is not validate") ;
        }
        // kiểm tra recurring interrest
        if(recurringInterest != "") {
            offerDTO.setRecurringInterest(recurringInterest);
        } else {
            throw  new ApplicationException(404,"Recurring Interest is not validate") ;
        }
        // lấy collateral theo id cho Offer
        CollateralDTO collateral = collateralService.findCollateralById(collateralId) ;
        if(collateral == null) {
            throw new ApplicationException(404,"Collateral is not found") ;
        }
        offerDTO.setCollateral(collateral);

        // set trạng thái ban đầu cho offer là OPEN
        offerDTO.setStatus(StatusOffer.OPEN.toString());

        offerService.save(offerDTO) ;
        return offerDTO;
    }

    /**
     * Lấy ra tất cả Offer
     * @param
     * @return
     */
    @GetMapping("/offers")
    public List<OfferDTO> getAllOffer() {

        List<OfferDTO> list = offerService.getAll() ;
        if(list.size() >0) {
            return list ;
        } else {
            throw new ApplicationException(404, "Can not find Offer List") ;
        }
    }

    /**
     * Lấy ra Offer có id chỉ định
     * @param offer_id
     * @return
     */
    @GetMapping("/offers/{offer_id}")
    public OfferDTO getOffer(@PathVariable long offer_id) {
        OfferDTO dto = offerService.findById(offer_id) ;
        if(dto != null) {
            return dto ;
        } else {
            throw new ApplicationException(404, "Can not find Offer List") ;
        }
    }

    /**
     * Accept một Offer cho một collateral
     * @param collateral_id , offer_id
     * @return
     */
    @PatchMapping("offer/{collateral_id}/accept/{offer_id}")
    public OfferDTO acceptOffer(@PathVariable long collateral_id,
                             @PathVariable long offer_id) {

        CollateralDTO collateralDTO = collateralService.findCollateralById(collateral_id) ;
        OfferDTO offerDTO = offerService.findById(offer_id) ;
        if(collateralDTO.getStatus().equals(StatusCollaterals.OPEN.toString())) {

            collateralDTO.setStatus(StatusCollaterals.ACCEPTED.toString());
            collateralDTO.getOfferList().forEach(offer -> {
                offer.setStatus(StatusOffer.REJECTED.toString()) ;
                offerService.save(offer);
            }) ;
            offerDTO.setStatus(StatusOffer.ACCEPTED.toString());
        } else {
            System.out.println(collateralDTO.getStatus()) ;
            throw new ApplicationException(1234,"Collateral not OPEN") ;
        }
        collateralService.create(collateralDTO);
        offerService.save(offerDTO);
        return offerDTO ;
    }



}
