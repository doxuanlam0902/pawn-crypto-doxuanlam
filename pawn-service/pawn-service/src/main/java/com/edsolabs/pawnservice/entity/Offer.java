package com.edsolabs.pawnservice.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    @Column
    private String lender;
    @Column
    private String status;
    @Column
    private String message;
    @Column
    private double loanToValue;
    @Column
    private int interestRate;
    @Column
    private int duration ;
    @Column
    private double LTV ;
    @Column
    private String recurringInterest ;
    @Column
    private String RepaymentCurrency ;
    @Column
    private Timestamp createdDay ;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collateral_id")
    private Collateral collateral;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "offer_crypto_id")
    private Crypto loanAmount;


}
