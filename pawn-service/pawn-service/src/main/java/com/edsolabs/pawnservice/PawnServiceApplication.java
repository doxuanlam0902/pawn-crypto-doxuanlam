package com.edsolabs.pawnservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PawnServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PawnServiceApplication.class, args);
	}

}
