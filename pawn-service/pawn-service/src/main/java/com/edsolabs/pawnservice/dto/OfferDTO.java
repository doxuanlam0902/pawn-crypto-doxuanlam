package com.edsolabs.pawnservice.dto;

import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Crypto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

@Getter
@Setter
public class OfferDTO {
    private long id ;
    @NotEmpty(message ="Lender not empty")
    private String lender;
    private String status;
    @NotEmpty(message ="message not empty")
    private String message;
    @NotEmpty(message ="loan to value not empty")
    private double loanToValue;
    @NotEmpty(message ="interest rate not empty")
    private int interestRate;
    @NotEmpty(message ="duration not empty")
    private int duration ;
    @NotEmpty(message ="LTV not empty")
    private double LTV ;
    @NotEmpty(message ="recurring interest not empty")
    private String recurringInterest ;
    @NotEmpty(message ="repayment currency not empty")
    private String repaymentCurrency ;
    private Timestamp createdDay ;
    private CollateralDTO collateral;
    private CryptoDTO loanAmount;
}
