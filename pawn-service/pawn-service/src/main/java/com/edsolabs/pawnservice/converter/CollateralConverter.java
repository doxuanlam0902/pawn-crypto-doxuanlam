package com.edsolabs.pawnservice.converter;

import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CollateralConverter {
    @Autowired
    CryptoConverter cryptoConverter ;
    @Autowired
    OfferConverter offerConverter ;

    public Collateral toCollateralDTO(CollateralDTO dto) {

        Collateral collateral = new Collateral() ;
        collateral.setId(dto.getId());
        collateral.setCollateralAmount(dto.getCollateralAmount());
        collateral.setDuration(dto.getDuration());
        collateral.setEstimate(dto.getEstimate());
        collateral.setMessage(dto.getMessage());
        collateral.setStatus(dto.getStatus());
        collateral.setLoanCurrency(cryptoConverter.toCryptoDTO(dto.getLoanCurrency()));
        collateral.setCrypto(cryptoConverter.toCryptoDTO(dto.getCryptoCurrency()));
        collateral.setOfferList(offerConverter.toListOfferDTO(dto.getOfferList()));
        System.out.println("id crypto in dto after converter " + collateral.getCrypto().getId());
        System.out.println("id loan in dto after converter " + collateral.getLoanCurrency().getId());

        return collateral ;
    }

    public CollateralDTO toCollateral(Collateral collateral) {
        CollateralDTO dto = new CollateralDTO() ;
        dto.setId(collateral.getId());
        dto.setCollateralAmount(collateral.getCollateralAmount()) ;
        dto.setDuration(collateral.getDuration());
        dto.setEstimate(collateral.getEstimate());
        dto.setMessage(collateral.getMessage());
        dto.setStatus(collateral.getStatus());
        dto.setLoanCurrency(cryptoConverter.toCrypto(collateral.getLoanCurrency()));
        dto.setCryptoCurrency(cryptoConverter.toCrypto(collateral.getCrypto()));
        dto.setOfferList(offerConverter.toListOffer(collateral.getOfferList()));
        return dto ;
    }

    public List<Collateral> toListCollateralDTO(List<CollateralDTO> dtoList) {
        List<Collateral> collateralList = new ArrayList<>() ;
        dtoList.forEach(dto -> {
            collateralList.add(this.toCollateralDTO(dto)) ;
        });

        return  collateralList ;
    }

    public List<CollateralDTO> toListCollateral(List<Collateral> collateralList) {
        List<CollateralDTO> dtoList = new ArrayList<>() ;
        collateralList.forEach(collateral -> {
            dtoList.add(this.toCollateral(collateral)) ;
        });

        return  dtoList ;
    }

    public CollateralDTO toCollateralforOffer(Collateral collateral) {
        CollateralDTO dto = new CollateralDTO() ;
        dto.setId(collateral.getId());
        dto.setCollateralAmount(collateral.getCollateralAmount()) ;
        dto.setDuration(collateral.getDuration());
        dto.setEstimate(collateral.getEstimate());
        dto.setMessage(collateral.getMessage());
        dto.setStatus(collateral.getStatus());
        dto.setLoanCurrency(cryptoConverter.toCrypto(collateral.getLoanCurrency()));
        dto.setCryptoCurrency(cryptoConverter.toCrypto(collateral.getCrypto()));
//        dto.setOfferList(offerConverter.toListOffer(collateral.getOfferList()));
        return dto ;
    }
}
