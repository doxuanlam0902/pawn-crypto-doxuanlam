package com.edsolabs.pawnservice.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "crypto_asset")
public class Crypto {
    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column()
    private String symbol ;

    @Column()
    private String address ;

    @Column()
    @CreatedDate
    private Timestamp createdAt ;

    @Column()
    @LastModifiedDate
    private Timestamp updateAt ;

    @Column()
    private Boolean isDeleted ;

    @Column()
    private Boolean whiteListCollateral ;

    @Column()
    private Boolean whiteListSupply ;

    @Column()
    private String name ;

    @Column()
    private String CoinGeckoId ;

    @Column()
    private String urlImage ;

    @OneToMany(mappedBy  = "crypto")
    private List<Collateral> collateralList = new ArrayList<>() ;

    @OneToMany(mappedBy = "loanCurrency")
    private List<Collateral> loanCollateralList = new ArrayList<>();

    @OneToMany(mappedBy = "loanAmount")
    private List<Offer> offerList = new ArrayList<>() ;

}