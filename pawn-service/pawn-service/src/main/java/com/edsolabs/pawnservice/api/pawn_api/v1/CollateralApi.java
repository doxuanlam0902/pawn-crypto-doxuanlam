package com.edsolabs.pawnservice.api.pawn_api.v1;



import com.edsolabs.pawnservice.StatusCollaterals;
import com.edsolabs.pawnservice.StatusOffer;
import com.edsolabs.pawnservice.api.output.ApplicationException;
import com.edsolabs.pawnservice.config.PageConfig;
import com.edsolabs.pawnservice.converter.CryptoConverter;
import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Crypto;
import com.edsolabs.pawnservice.repository.OfferRepository;
import com.edsolabs.pawnservice.service.CollateralService;
import com.edsolabs.pawnservice.service.OfferService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static java.sql.Types.NULL;

@RestController
@Slf4j
@RequestMapping("/api/v1")
@CrossOrigin
public class CollateralApi {

    @Autowired
    CollateralService collateralService ;

    @Autowired
    CryptoConverter cryptoConverter ;

    @Autowired
    OfferService offerService ;
    /**
     * tạo mới một collateral
     * @param collateralAmount, currency, message, duration, loanCurrency
     * @return
     */
    @PostMapping(value = "/collaterals")
    public CollateralDTO createCollateral(@RequestParam(name = "collateralAmount") int collateralAmount ,
                                       @RequestParam(name = "currency") Long currencyId,
                                       @RequestParam(name = "message") String message,
                                       @RequestParam(name = "duration") int duration,
                                       @RequestParam(name = "loanCurrency") Long loanCurrencyId){
        CollateralDTO collateralDTO = new CollateralDTO() ;
        // kiểm tra collateral amount, nếu > 0 thì chấp nhận
        if(collateralAmount > 0) {
            collateralDTO.setCollateralAmount(collateralAmount);
        } else {
            throw new ApplicationException(404 , "Collateral Amount is null") ;
        }
        // kiểm tra message
        if(message != "") {
            collateralDTO.setMessage(message);
        } else {
            throw new ApplicationException(404 , "Message of Collateral is not validate") ;
        }
        // kiểm tra duration
        if(duration != NULL) {
            collateralDTO.setDuration(duration);
        } else {
            throw new ApplicationException(404 , "Duration is not validate") ;
        }
        // đặt trạng thái ban đầu cho collateral là OPEN
        collateralDTO.setStatus(StatusCollaterals.OPEN.toString());

        // lấy ra crypto cho crypto currency
        RestTemplate restTemplate = new RestTemplate();
        Crypto currency = restTemplate.getForObject(
                "http://localhost:9000/api/v1/crypto/find/" + currencyId ,
                Crypto.class);
        if(currency != null && currency.getWhiteListCollateral()) {
            collateralDTO.setCryptoCurrency(cryptoConverter.toCrypto(currency));
            System.out.println("id cryptodto"+cryptoConverter.toCrypto(currency).getId()) ;
        } else {
            throw new ApplicationException(404 , "currency is not validate") ;
        }

        // lấy ra crypto cho Loan currency
        Crypto loanCurrency = restTemplate.getForObject(
                "http://localhost:9000/api/v1/crypto/find/" + loanCurrencyId ,
                Crypto.class);
        if(loanCurrency != null && loanCurrency.getWhiteListSupply()) {
            collateralDTO.setLoanCurrency(cryptoConverter.toCrypto(loanCurrency));
        } else {
            throw new ApplicationException(404 , "Loan Currency is not validate") ;
        }

        // lấy tỉ ra hiện tại của crypto
        Date time = new Date();
        Timestamp timestamp = new Timestamp(time.getTime());
        System.out.println(timestamp.getTime());
        Double cur = restTemplate.getForObject(
                "http://localhost:9001/api/v1/crypto/find-rate-by-timeAt?symbol="+loanCurrency.getSymbol()+"&time=" + timestamp.getTime() ,
                Double.class);
        if(cur != null) {
            // ước tính = tỉ giá * số lượng
            collateralDTO.setEstimate( cur * collateralAmount );
        } else {
            throw new ApplicationException(404 , "Estimate is not find") ;
        }

        // đặt trạng thái ban đầu cho collateral
        collateralDTO.setStatus(StatusCollaterals.OPEN.toString());

        log.info("Inside createCollateral at CollateralAPI");
        collateralService.create(collateralDTO);
        return collateralDTO ;
    }

    /**
     * Lấy ra collateral có id chỉ định
     * @param collateral_id
     * @return
     */
    @GetMapping(value = "/collaterals/{collateral_id}")
    public CollateralDTO showCollateralById(@PathVariable Long collateral_id) {
        log.info("Inside showCollateralById at CollateralAPI");
        CollateralDTO dto = collateralService.findCollateralById(collateral_id) ;
        if(dto != null){
            return dto ;
        } else {
            throw  new ApplicationException(400 ,"Not Find Collaterals with id = " + collateral_id);
        }
    }

    /**
     * Lấy ra tất cả collateral
     * @param page, limit
     * @return
     */
    @GetMapping(value = "/collaterals")
    public Page<CollateralDTO> showAllCollateral(
            @RequestParam(name ="page") int page,
            @RequestParam(name ="limit") int limit) {
        log.info("Inside showAllCollateral at CollateralAPI");


        PageConfig<CollateralDTO> pageConfig = new PageConfig<>() ;
        Pageable pageable = PageRequest.of(page - 1, limit, Sort.by("id").ascending());
        Page<CollateralDTO> list = collateralService.findAllByStatus(pageable) ;
        return list ;

    }

    /**
     * withdraw một collateral
     * @param id
     * @return
     */
    @PatchMapping(value = "/withdrawn/{id}")
    public void withdrawCollateral(@PathVariable long id){
        CollateralDTO collateralDTO = collateralService.findCollateralById(id) ;
        // nếu trạng thái khác OPEN thì ko withdrawn
        if(collateralDTO.getStatus().equals(StatusCollaterals.OPEN.toString())){
            collateralDTO.setStatus(StatusCollaterals.WITHDRAW.toString());
            collateralDTO.getOfferList().forEach(dto ->{
                dto.setStatus("CANCELED");
                offerService.save(dto);
            });
            collateralService.create(collateralDTO);
        } else{
            throw new ApplicationException(1234,"Collateral not OPEN") ;
        }

    }


//    @GetMapping
//    @ApiOperation(value = "Show List Collateral For User")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
//                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
//            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
//                    value = "Number of records per page.", defaultValue = "15"),
//            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
//                    paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
//                    + "Default sort order is ascending. Multiple sort criteria are supported.")})
//    public ApiResponse<?> getListCollateralForMyAccount(
//            @RequestParam String walletAddress,
//            @RequestParam(required = false) String supplyCurrencySymbol,
//            @RequestParam(required = false) String collateralCurrencySymbol,
//            @RequestParam(required = false) Integer status,
//            @RequestParam(required = false) Long userId,
//            @ApiIgnore Pageable pageable
//    ){
//
//    }
}
