package com.edsolabs.pawnservice.api.output;

import lombok.Getter;

@Getter

public class StatusNotValidException extends RuntimeException{

    private String mess  ;
}
