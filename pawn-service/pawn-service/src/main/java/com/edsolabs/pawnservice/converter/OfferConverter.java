package com.edsolabs.pawnservice.converter;

import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.dto.OfferDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
public class OfferConverter {
    @Autowired
    CollateralConverter collateralConverter ;
    @Autowired
    CryptoConverter cryptoConverter ;
    public OfferDTO toOffer(Offer offer) {
        OfferDTO dto = new OfferDTO() ;
        // converter entity to dto
        dto.setId(offer.getId());
        dto.setLender(offer.getLender());
        dto.setStatus(offer.getStatus());
        dto.setMessage(offer.getMessage());
        dto.setLoanToValue(offer.getLoanToValue());
        dto.setInterestRate(offer.getInterestRate());
        dto.setDuration(offer.getDuration());
        dto.setLTV(offer.getLTV());
        dto.setRecurringInterest(offer.getRecurringInterest());
        dto.setRepaymentCurrency(offer.getRepaymentCurrency());
        dto.setCreatedDay(offer.getCreatedDay());
        dto.setCollateral(collateralConverter.toCollateralforOffer(offer.getCollateral()));
        dto.setLoanAmount(cryptoConverter.toCrypto(offer.getLoanAmount()));
        return dto ;

    }

    public Offer toOfferDTO(OfferDTO dto) {
        Offer offer = new Offer() ;
        // converter dto to entity
        offer.setId(dto.getId());
        offer.setLender(dto.getLender());
        offer.setStatus(dto.getStatus());
        offer.setMessage(dto.getMessage());
        offer.setLoanToValue(dto.getLoanToValue());
        offer.setInterestRate(dto.getInterestRate());
        offer.setDuration(dto.getDuration());
        offer.setLTV(dto.getLTV());
        offer.setRecurringInterest(dto.getRecurringInterest());
        offer.setRepaymentCurrency(dto.getRepaymentCurrency());
        offer.setCreatedDay(dto.getCreatedDay());
        offer.setCollateral(collateralConverter.toCollateralDTO(dto.getCollateral()));
        offer.setLoanAmount(cryptoConverter.toCryptoDTO(dto.getLoanAmount()));
        return offer ;

    }

    public List<Offer> toListOfferDTO(List<OfferDTO> dtoList) {
        List<Offer> offerList = new ArrayList<>() ;
        dtoList.forEach(dto -> {
            offerList.add(this.toOfferDTO(dto)) ;
        });

        return  offerList ;
    }

    public List<OfferDTO> toListOffer(List<Offer> offerList) {
        List<OfferDTO> dtoList = new ArrayList<>() ;
        offerList.forEach(offer -> {
            dtoList.add(this.toOffer(offer)) ;
        });

        return  dtoList ;
    }

}
