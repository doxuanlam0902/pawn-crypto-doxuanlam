package com.edsolabs.pawnservice.dto;

import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Offer;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CryptoDTO {
    private Long id ;
    private String symbol ;
    private String address ;
    private Timestamp createdAt ;
    private Timestamp updateAt ;
    private Boolean isDeleted ;
    private Boolean whiteListCollateral ;
    private Boolean whiteListSupply ;
    private String name ;
    private String CoinGeckoId ;
    private String urlImage ;

}
