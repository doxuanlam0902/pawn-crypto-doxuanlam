package com.edsolabs.pawnservice.service;

import com.edsolabs.pawnservice.api.output.ApplicationException;
import com.edsolabs.pawnservice.converter.CollateralConverter;
import com.edsolabs.pawnservice.converter.OfferConverter;
import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.dto.OfferDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.entity.Offer;
import com.edsolabs.pawnservice.repository.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class OfferService {

    @Autowired
    OfferRepository offerRepository ;

    @Autowired
    OfferConverter offerConverter ;
    @Autowired
    CollateralConverter collateralConverter ;

    public void save(OfferDTO dto) {

        Offer offer = offerConverter.toOfferDTO(dto) ;
        offer.setCollateral(collateralConverter.toCollateralDTO(dto.getCollateral()));
        offer.getCollateral().getOfferList().add(offer) ;
        Date time = new Date();
        Timestamp timestamp = new Timestamp(time.getTime());
        System.out.println(timestamp.getTime());
        offer.setCreatedDay(timestamp);

        offerRepository.save(offer) ;
    }

    public List<OfferDTO> getAll(){
        return offerConverter.toListOffer(offerRepository.findAll());
    }

    public OfferDTO findById(long id){
        OfferDTO dto = offerConverter.toOffer(offerRepository.findById(id).orElse(null)) ;
        if (dto == null) {
            throw new ApplicationException(404,"Can not find offer with id = " + id) ;
        }
        return dto ;
    }
}
