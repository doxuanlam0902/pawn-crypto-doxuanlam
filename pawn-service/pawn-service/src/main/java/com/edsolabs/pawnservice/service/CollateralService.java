package com.edsolabs.pawnservice.service;


import com.edsolabs.pawnservice.api.output.ApplicationException;
import com.edsolabs.pawnservice.converter.CollateralConverter;
import com.edsolabs.pawnservice.dto.CollateralDTO;
import com.edsolabs.pawnservice.entity.Collateral;
import com.edsolabs.pawnservice.repository.CollateralRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.List;

@Service
public class CollateralService {
    @Autowired
    CollateralRepository collateralRepository ;
    @Autowired
    CollateralConverter collateralConverter ;

    public void create(@Valid CollateralDTO collateralDTO) {
        System.out.println("id crypto in dto before save " + collateralDTO.getCryptoCurrency().getId());
        System.out.println("id loan in dto before save " + collateralDTO.getLoanCurrency().getId());
        Collateral collateral = collateralConverter.toCollateralDTO(collateralDTO) ;
        System.out.println("id crypto in entity " + collateral.getCrypto().getId());
        System.out.println("id loan in entity " + collateral.getLoanCurrency().getId());
        collateralRepository.save(collateral) ;
    }

    public CollateralDTO findCollateralById(Long id){
        Collateral collateral =  collateralRepository.findById(id).orElse(null) ;
        if(collateral == null){
            throw new ApplicationException(404, "Not Find Collateral") ;
        }
        return collateralConverter.toCollateral(collateral) ;
    }

    public Page<CollateralDTO> findAllByStatus(Pageable pageable){
        Page<Collateral> collateralListPage = collateralRepository.findAll(pageable) ;
        Page<CollateralDTO> collateralDTOList = new PageImpl<CollateralDTO>(collateralConverter.toListCollateral(collateralListPage.getContent())) ;

//        Page<CollateralDTO> dtoList = collateralList.map(collateralConverter.toListCollateral(collateralList.));
        return collateralDTOList;
    }

}
