package com.edsolabs.pawnservice.config;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PageConfig<T> {
    private int page ;
    private int totalPage ;
    private List<T> listResults = new ArrayList<>() ;
}
