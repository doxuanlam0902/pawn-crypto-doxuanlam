package com.edsolabs.pawnservice.repository;

import com.edsolabs.pawnservice.entity.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface OfferRepository extends JpaRepository<Offer , Long> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE Collateral SET Collateral.status = 'WITHDRAWN' WHERE Collateral.id = ?1",nativeQuery = true)
    void withdrawnCollateral(long id) ;
}
