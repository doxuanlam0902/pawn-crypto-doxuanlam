package com.edsolabs.pawnservice.entity;

import com.edsolabs.pawnservice.StatusOffer;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Collateral {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id ;
    @Column
    @NotNull(message = "Collateral Amount is NULL")
    private double collateralAmount ; // Số lương thế chấp
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_crypto_id")
    @NotNull(message = "Loan Currency is NULL")
    private Crypto loanCurrency;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "crypto_id")
    @NotNull(message = "Crypto Amount is NULL")
    private Crypto crypto;
    @Column
//    @NotEmpty(message = "Message is empty")
    private String message ;
    @Column

    private int duration ; // thời hạn - tính theo tuần hoặc tháng

    @Column
    private double estimate ; // Tỉ Giá
//    private double currency ;
    @Column
    private String status ;
    @OneToMany(mappedBy = "collateral")
    private List<Offer> offerList = new ArrayList<>();




}
