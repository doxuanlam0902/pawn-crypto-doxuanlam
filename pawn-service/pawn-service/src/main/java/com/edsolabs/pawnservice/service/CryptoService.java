package com.edsolabs.pawnservice.service;

import com.edsolabs.pawnservice.converter.CryptoConverter;
import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.entity.Crypto;
import com.edsolabs.pawnservice.repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CryptoService {

    @Autowired
    CryptoRepository cryptoRepository ;
    @Autowired
    CryptoConverter cryptoConverter ;

    public CryptoDTO findCryptoById(Long id) {
        return cryptoConverter.toCrypto(cryptoRepository.findCryptoById(id)) ;
    }

    public List<CryptoDTO> findALl() {
        return cryptoConverter.toCryptoList(cryptoRepository.findAll()) ;
    }
}
