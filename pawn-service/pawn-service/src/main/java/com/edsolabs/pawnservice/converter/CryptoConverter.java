package com.edsolabs.pawnservice.converter;

import com.edsolabs.pawnservice.dto.CryptoDTO;
import com.edsolabs.pawnservice.entity.Crypto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CryptoConverter {
    public Crypto toCryptoDTO(CryptoDTO dto){
        //converter dto -> entity
        Crypto crypto = new Crypto();
        if (dto.getId() != null) {
            crypto.setId(dto.getId());
        }
        crypto.setSymbol(dto.getSymbol());
        crypto.setAddress(dto.getAddress());
        crypto.setCreatedAt(dto.getCreatedAt());
        crypto.setUpdateAt(dto.getUpdateAt());
//        entity.setIsDelete(dto.getIsDelete());
        crypto.setIsDeleted(dto.getIsDeleted());
        crypto.setIsDeleted(dto.getIsDeleted());
        crypto.setWhiteListSupply(dto.getWhiteListSupply());
        crypto.setWhiteListCollateral(dto.getWhiteListCollateral());
        crypto.setName(dto.getName());
        crypto.setCoinGeckoId(dto.getCoinGeckoId());
        crypto.setUrlImage(dto.getUrlImage());
        //
        return crypto ;
    }

    public CryptoDTO toCrypto(Crypto crypto){
        CryptoDTO dto = new CryptoDTO() ;
        //converter entity -> dto
        if (crypto.getId() != null) {
            dto.setId(crypto.getId());
        }
        dto.setSymbol(crypto.getSymbol());
        dto.setAddress(crypto.getAddress());
        dto.setCreatedAt(crypto.getCreatedAt());
        dto.setUpdateAt(crypto.getUpdateAt());
//        dto.setIsDelete(entity.getIsDelete());
        dto.setIsDeleted(crypto.getIsDeleted());
        dto.setWhiteListSupply(crypto.getWhiteListSupply());
        dto.setWhiteListCollateral(crypto.getWhiteListCollateral());
        dto.setName(crypto.getName());
        dto.setCoinGeckoId(crypto.getCoinGeckoId());
        dto.setUrlImage(crypto.getUrlImage());
        return dto;
    }

    public List<CryptoDTO> toCryptoList(List<Crypto> cryptoList) {
        List<CryptoDTO> dtoList = new ArrayList<>() ;
        cryptoList.forEach(crypto -> {
            dtoList.add(this.toCrypto(crypto)) ;
        });

        return dtoList ;
    }
}