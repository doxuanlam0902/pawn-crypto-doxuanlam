package com.edsolabs.pawnservice.repository;

import com.edsolabs.pawnservice.entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long> {

    Crypto findCryptoById(Long id) ;
}
