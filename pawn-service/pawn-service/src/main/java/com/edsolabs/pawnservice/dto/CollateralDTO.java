package com.edsolabs.pawnservice.dto;

import com.edsolabs.pawnservice.StatusCollaterals;
import com.edsolabs.pawnservice.entity.Crypto;
import com.edsolabs.pawnservice.entity.Offer;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CollateralDTO {
    private Long id ;
    @NotNull(message = "Collateral Amount is NULL")
    private double collateralAmount ;
    @NotNull(message = "Currency is NULL")
    private CryptoDTO cryptoCurrency ;
    @NotNull(message = "Loan Collateral is NULL")
    private CryptoDTO loanCurrency ;
    @NotEmpty(message = "Message is NULL")
    private String message ;
    @NotEmpty(message = "Duration is NULL")
    private int duration ;
    private double estimate ;

    //    private double currency ;
    private String status ;
    private int offer ;
    private List<OfferDTO> offerList = new ArrayList<>();



}
