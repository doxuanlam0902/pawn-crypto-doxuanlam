package com.edsolabs.pawnservice.api.output;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApplicationException extends RuntimeException {
    private int code;
    private String message;
}
