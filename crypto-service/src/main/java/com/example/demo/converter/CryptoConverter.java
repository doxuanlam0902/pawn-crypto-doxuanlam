package com.example.demo.converter;

import com.example.demo.dto.CryptoDTO;
import com.example.demo.domain.Crypto;
import org.springframework.stereotype.Component;


@Component
public class CryptoConverter {
    public Crypto DTOtoEntity(CryptoDTO dto){
        //converter dto -> entity
        Crypto crypto = new Crypto();
        crypto.setSymbol(dto.getSymbol());
        crypto.setAddress(dto.getAddress());
        crypto.setCreatedAt(dto.getCreatedAt());
        crypto.setUpdateAt(dto.getUpdatedAt());
//        entity.setIsDelete(dto.getIsDelete());
        crypto.setIsDeleted(dto.getIsDelete());
        crypto.setIsDeleted(dto.getIsDelete());
        crypto.setWhiteListSupply(dto.getWhitelistSupply());
        crypto.setWhiteListCollateral(dto.getWhitelistCollateral());
        crypto.setName(dto.getName());
        crypto.setCoinGeckoId(dto.getCoinGeckoId());

        //
        return crypto ;
    }

    public CryptoDTO EntitytoDTO(Crypto crypto){
        CryptoDTO dto = new CryptoDTO() ;
        //converter entity -> dto
        if (crypto.getId() != null) {
            dto.setId(crypto.getId());
        }
        dto.setSymbol(crypto.getSymbol());
        dto.setAddress(crypto.getAddress());
        dto.setCreatedAt(crypto.getCreatedAt());
        dto.setUpdatedAt(crypto.getUpdateAt());
//        dto.setIsDelete(entity.getIsDelete());
        dto.setIsDelete(crypto.getIsDeleted());
        dto.setWhitelistSupply(crypto.getWhiteListSupply());
        dto.setWhitelistCollateral(crypto.getWhiteListCollateral());
        dto.setName(crypto.getName());
        dto.setCoinGeckoId(crypto.getCoinGeckoId());
        return dto;
    }
}
