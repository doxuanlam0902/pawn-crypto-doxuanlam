package com.example.demo.repository;

import com.example.demo.domain.Crypto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long>, CrudRepository<Crypto, Long> {

    @Query(
            value = "SELECT * FROM crypto_asset c WHERE c.symbol like %?1% and c.address like %?2% and c.name like %?3%",
            nativeQuery = true)
    List<Crypto> findCrypto(String symbol, String address, String name) ;

    List<Crypto> findAllByIsDeleted(Boolean isDelete, Pageable pageable);
    Crypto findBySymbolIgnoreCase(String symbol) ;




}
