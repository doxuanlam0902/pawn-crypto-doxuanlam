package com.example.demo.repository;

import com.example.demo.domain.Crypto;
import com.example.demo.domain.Crypto_history_rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface CryptoHistoryRateRepository extends JpaRepository<Crypto_history_rate, Long>, CrudRepository<Crypto_history_rate,Long> {

    @Modifying
    @Transactional
    @Query(value =
            "DELETE FROM Crypto_history_rate c WHERE c.timeAt < ?1 " )
    void deleteHistoryAfter60min(Timestamp timestamp) ;

    @Query(value =
                "SELECT * FROM crypto_history_rate WHERE crypto_history_rate.time_at < ?2 AND crypto_history_rate.crypto_id = ?1 ORDER BY crypto_history_rate.rate desc limit 1", nativeQuery = true)
    Crypto_history_rate findRateByTimeAt(Long id, Timestamp timestamp) ;



//    void findRateByTimeAt(String symbol , Timestamp timestamp) ;

}
