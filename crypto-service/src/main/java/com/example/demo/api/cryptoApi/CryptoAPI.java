package com.example.demo.api.cryptoApi;


import com.example.demo.dto.CryptoDTO;
import com.example.demo.dto.GeckoDTO;
import com.example.demo.repository.CryptoHistoryRateRepository;
import com.example.demo.service.CryptoHistoryRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import com.example.demo.api.output.CryptoOutput;
import com.example.demo.domain.Crypto;
import com.example.demo.service.CryptoService;
import com.example.demo.service.ServiceResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/crypto")
public class CryptoAPI {
    @Autowired
    private CryptoService cryptoService;
    @Autowired
    private CryptoHistoryRateService cryptoHistoryRateService ;
    /* ---------------- GET ALL CUSTOMER ------------------------ */
//    @GetMapping("/cryptos")
//    public ResponseEntity<ServiceResult> findAllCustomer() {
//        return new ResponseEntity<ServiceResult>(cryptoService.findAll(), HttpStatus.OK);
//    }

    //    //Lay cac Crypto co su dung phan trang
        @GetMapping("/cryptos")
    public CryptoOutput getCryptos(@RequestParam("page") int page,
                                    @RequestParam("limit") int limit) {
        CryptoOutput cryptoOutput = new CryptoOutput() ;
        Pageable pageable = PageRequest.of(page - 1, limit, Sort.by("symbol").ascending());
        cryptoOutput.setPage(page);
        cryptoOutput.setListResults(cryptoService.findAll(pageable));
        cryptoOutput.setTotalPage((int) Math.ceil((double) (cryptoService.totalItem()) / limit));
        return cryptoOutput;
    }

    @GetMapping("/cryptos/find_crypto")
    public ResponseEntity<ServiceResult> findCrypto(@RequestParam(name = "symbol", required = false) String symbol,
                                                    @RequestParam(name = "address", required = false) String address,
                                                    @RequestParam(name = "name", required = false) String name,
                                                    @RequestParam("page") int page,
                                                    @RequestParam("limit")int limit) {
        String symbolQuery = "" ;
        String addressQuery = "" ;
        String nameQuery = "" ;

        if(symbol != null){
            symbolQuery = symbol ;
        }
        if(address !=null){
            addressQuery = address ;
        }
        if(name != null ){
            nameQuery = name ;
        }
//        CryptoOutput cryptoOutput = new CryptoOutput() ;
//        Pageable pageable = PageRequest.of(page - 1, limit, Sort.by("symbol").ascending());
//        cryptoOutput.setPage(page);
//        cryptoOutput.setListResults(cryptoService.findAll(pageable));
//        cryptoOutput.setTotalPage((int) Math.ceil((double) (cryptoService.totalItem()) / limit));
        return new ResponseEntity<ServiceResult>(cryptoService.findCrypto(symbolQuery, addressQuery, nameQuery), HttpStatus.OK);
    }
    /* ---------------- GET CUSTOMER BY ID ------------------------ */
    @GetMapping("/cryptos/{id}")
    public ResponseEntity<ServiceResult> findById(@PathVariable long id) {
        return new ResponseEntity<ServiceResult>(cryptoService.findById(id), HttpStatus.OK);
    }
    /* ---------------- CREATE NEW CUSTOMER ------------------------ */
    @PostMapping("/cryptos")
    public ResponseEntity<ServiceResult> create(@RequestBody Crypto crypto) {
        return new ResponseEntity<ServiceResult>(cryptoService.create(crypto), HttpStatus.OK);
    }

    /* ---------------- UPDATE CUSTOMER ------------------------ */
    @PutMapping("/cryptos")
    public ResponseEntity<ServiceResult> update(@RequestBody Crypto crypto) {
        return new ResponseEntity<ServiceResult>(cryptoService.update(crypto), HttpStatus.OK);
    }
    @DeleteMapping("/cryptos")
    public ResponseEntity<ServiceResult> delete(@RequestBody DeleteCryptoRequest request) {
        return new ResponseEntity<ServiceResult>(cryptoService.delete(request.getId()), HttpStatus.OK);
    }

    @GetMapping(value = "/crypto/price")
    public double currentPrice(@RequestParam("symbol") String symbol) {
        GeckoDTO geckoDTO = cryptoService.getCryptoCurrency(symbol) ;
        if(geckoDTO == null){
            return 0 ;
        }
        return geckoDTO.getCurrent_price();
    }

    @GetMapping("/convert-crypto")
    public double changeCurrency(@RequestParam("formSymbol") Crypto formCrypto ,
                                 @RequestParam("toSymbol") Crypto toCrypto,
                                 @RequestParam double formAmount) {
        if(formCrypto == null || toCrypto == null) {
            return 0 ;
        }
        return cryptoService.changeCurrencyCrypto(formCrypto, formAmount , toCrypto) ;
    }

    @GetMapping("/find-rate-by-timeAt")
    public double findRateByTimeAt (@RequestParam String symbol,
                                    @RequestParam long time){
        Timestamp timestamp = new Timestamp(time) ;
        CryptoDTO cryptoDTO = cryptoService.findBySymbol(symbol) ;
        return cryptoHistoryRateService.findByTimeAt(cryptoDTO.getId(),timestamp);

    }
}
