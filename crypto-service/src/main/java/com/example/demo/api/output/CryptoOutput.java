package com.example.demo.api.output;

import com.example.demo.dto.CryptoDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CryptoOutput {
    private int page ;
    private int totalPage ;
    private List<CryptoDTO> listResults = new ArrayList<>() ;

}
