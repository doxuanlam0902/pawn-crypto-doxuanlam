package com.example.demo.api.cryptoApi;
/*
    Ngoài trừ API GET, dữ liệu được truyền qua path của URL thì tất cả dữ liệu của các API DELETE, PUT, POST đều phải là dữ liệu JSON.
 */
public class DeleteCryptoRequest {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
