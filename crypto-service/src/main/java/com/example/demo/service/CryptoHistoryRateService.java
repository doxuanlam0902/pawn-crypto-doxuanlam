package com.example.demo.service;

import com.example.demo.domain.Crypto_history_rate;
import com.example.demo.repository.CryptoHistoryRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Service
public class CryptoHistoryRateService {

    @Autowired
    CryptoHistoryRateRepository cryptoHistoryRateRepository ;

    public void deleteHistoryAfter60min() {
//        System.out.println("Delete History after 60 min ");
//        Calendar calendar = Calendar.getInstance();
////        calendar.setTime(date);
//        calendar.add(Calendar.MINUTE , -60);
//        Date date = calendar.getTime() ;
//        Timestamp timestamp = new Timestamp(date.getTime())  ;
//        System.out.println("giá trị của calender" + calendar.getTime() );
//        System.out.println("giá trị của date : "+ date.getTime());
//        System.out.println("Giá trị của timestamp:"  + timestamp);
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.MINUTE, -60);
        Date previousYear = cal.getTime();
        Timestamp timestamp = new Timestamp(previousYear.getTime());
        cryptoHistoryRateRepository.deleteHistoryAfter60min(timestamp);
    }

    public void save(Crypto_history_rate crypto_history_rate) {
        if(crypto_history_rate == null) {
            return ;
        }
        cryptoHistoryRateRepository.save(crypto_history_rate) ;
    }

    public double findByTimeAt(Long id, Timestamp timestamp) {
        if(id == null) {
            return 0 ;
        }
        return cryptoHistoryRateRepository.findRateByTimeAt(id,timestamp).getRate();
    }
}
