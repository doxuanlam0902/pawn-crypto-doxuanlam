package com.example.demo.service;


import com.example.demo.converter.CryptoConverter;
import com.example.demo.domain.Crypto;
import com.example.demo.dto.CryptoDTO;
import com.example.demo.dto.GeckoDTO;
import com.example.demo.repository.CryptoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class CryptoService {

    @Autowired
    CryptoRepository cryptoRepository ;

    @Autowired
    CryptoConverter cryptoConverter;

    public List<Crypto> findAll(){
        List<Crypto> cryptos = cryptoRepository.findAll();
        return cryptos ;
    }

    public List<CryptoDTO> findAll (Pageable pageable) {
        List<CryptoDTO> results = new ArrayList<>() ;
        List<Crypto> cryptos = cryptoRepository.findAllByIsDeleted(false, pageable) ;
        cryptos.forEach(crypto -> results.add(cryptoConverter.EntitytoDTO(crypto)));
        return results ;
//        ServiceResult result = new ServiceResult() ;
//        result.setData(cryptoRepository.findAll());
//        return result ;
    }

    public ServiceResult findCrypto (String symbol, String address, String name) {
        ServiceResult result = new ServiceResult() ;
        result.setData(cryptoRepository.findCrypto(symbol, address, name));
        return result ;
    }
    public CryptoDTO findBySymbol(String symbol) {
        CryptoDTO cryptoDTO  ;
        Crypto crypto = cryptoRepository.findBySymbolIgnoreCase(symbol) ;
        cryptoDTO =  cryptoConverter.EntitytoDTO(crypto) ;
        return cryptoDTO ;
    }

    public ServiceResult findById(Long id) {
        ServiceResult result = new ServiceResult() ;
        Crypto crypto = cryptoRepository.findById(id).orElse(null) ;
        result.setData(crypto);
        return result ;
    }

    public ServiceResult create(Crypto crypto) {
        ServiceResult result = new ServiceResult();
        result.setData(cryptoRepository.save(crypto));
        return result;
    }

    public ServiceResult update(Crypto crypto) {
        ServiceResult result = new ServiceResult() ;

        if(!cryptoRepository.findById(crypto.getId()).isPresent()) {
            result.setStatus(ServiceResult.Status.FAILED);
            result.setData("Crypto Not Found");
        } else {
            result.setData(cryptoRepository.save(crypto));
        }
        return  result ;
    }

    public ServiceResult delete(long id) {
        ServiceResult result = new ServiceResult() ;
        Crypto crypto = cryptoRepository.findById(id).orElse(null) ;
        if(crypto == null) {
            result.setStatus(ServiceResult.Status.FAILED);
            result.setMessage("Crypto Not Found");
        } else {
            cryptoRepository.delete(crypto);
            result.setMessage("Success");
        }
        return result;
    }

    public int totalItem() {
        return (int) cryptoRepository.count() ;
    }


    public GeckoDTO getCryptoCurrency(String symbol) {

        Crypto crypto = cryptoRepository.findBySymbolIgnoreCase(symbol) ;
        if(crypto == null) {
            return null;
        }
        String url1 = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=";
        String url2 = "&order=market_cap_desc&per_page=100&page=1&sparkline=false";
        String url = url1 + crypto.getName().toLowerCase() + url2;
        RestTemplate restTemplate = new RestTemplate();
        GeckoDTO[] list = restTemplate.getForObject(url, GeckoDTO[].class);
        if (list != null) {
            return list[0];
        }
        return null ;
    }

    public double changeCurrencyCrypto(Crypto fromSymbol, double formAmount, Crypto toSymbol ){
        GeckoDTO fromDTO = getCryptoCurrency(fromSymbol.getSymbol()) ;
        GeckoDTO toDTO = getCryptoCurrency(toSymbol.getSymbol()) ;
        if(fromDTO != null && toDTO != null) {
            return formAmount * (fromDTO.getCurrent_price() / toDTO.getCurrent_price()) ;
        }
        return 0 ;
    }
}