package com.example.demo.domain;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Setter
@Getter
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "crypto_asset")
@Data
public class Crypto {
    @Id
    @Column(name ="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column()
    private String symbol ;

    @Column()
    private String address ;

    @Column()
    @CreatedDate
    private Timestamp createdAt ;

    @Column()
    @LastModifiedDate
    private Timestamp updateAt ;

    @Column()
    private Boolean isDeleted ;

    @Column()
    private String whiteListCollateral ;

    @Column()
    private String whiteListSupply ;

    @Column()
    private String name ;

    @Column()
    private String CoinGeckoId ;

    @OneToMany(mappedBy = "crypto")
    private List<Crypto_history_rate> cryptoHistoryRates = new ArrayList<>() ;





}
