package com.example.demo.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "crypto_history_rate")
@Getter
@Setter
@Data
public class Crypto_history_rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column
    @CreatedDate
    private Timestamp timeAt ;

    @Column
    private double rate ;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "crypto_id")
    private Crypto crypto ;


}
