package com.example.demo.config;


import com.example.demo.converter.CryptoConverter;
import com.example.demo.domain.Crypto;
import com.example.demo.domain.Crypto_history_rate;
import com.example.demo.dto.GeckoDTO;
import com.example.demo.repository.CryptoHistoryRateRepository;
import com.example.demo.service.CryptoHistoryRateService;
import com.example.demo.service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Configuration
@EnableScheduling
@EnableAsync
public class SpringConfig{


    @Autowired
    CryptoHistoryRateService cryptoHistoryRateService ;

    @Autowired
    CryptoService cryptoService ;

    @Scheduled(fixedDelay = 60000)
    public void getExchangeRate() {

        List<Crypto> cryptoList = cryptoService.findAll() ;
        System.out.println(cryptoList.size());
        if(cryptoList != null) {
            cryptoList.forEach(crypto -> {
                System.out.println("name of crypto :" + crypto.getName());
                String url1 = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=";
                String url2 = "&order=market_cap_desc&per_page=100&page=1&sparkline=false";
                String url = url1 + crypto.getName().toLowerCase() + url2;
                RestTemplate restTemplate = new RestTemplate();
                GeckoDTO[] geckoDTOS = restTemplate.getForObject(url, GeckoDTO[].class);
                if (geckoDTOS.length > 0) {
                    Crypto_history_rate crypto_history_rate = new Crypto_history_rate();
                    crypto_history_rate.setCrypto(crypto);
                    crypto_history_rate.setRate(geckoDTOS[0].getCurrent_price());
                    Date time = new Date();
                    Timestamp timestamp = new Timestamp(time.getTime());
                    System.out.println(timestamp.getTime());
                    crypto_history_rate.setTimeAt(timestamp);
                    cryptoHistoryRateService.save(crypto_history_rate);
                }

            });
            System.out.println("Lấy tỉ giá mỗi 1p");
        }

    }

    @Async
    @Scheduled(fixedRate = 60000)
    public void scheduleFixedRateTaskAsync() throws InterruptedException {
        cryptoHistoryRateService.deleteHistoryAfter60min();

    }

    //phuong thuc dc thuc hien luc 12h
//    @Scheduled(cron = "0 0 12 * * ?")
//    public void scheduleTaskUsingCronExpression() {
//
//        long now = System.currentTimeMillis() / 1000;
//        System.out.println(
//                "schedule tasks using cron jobs - " + now);
//    }
}
