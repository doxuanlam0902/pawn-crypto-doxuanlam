package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

@Getter
@Setter
public class CryptoDTO {
    private Long id;
    private String symbol;
    private String address;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Boolean isDelete;
    private String whitelistCollateral;
    private String whitelistSupply;
    private String name;
    private String coinGeckoId;
}
